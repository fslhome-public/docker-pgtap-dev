FROM debian:buster
MAINTAINER Thibaut Smith <thibaut.smith@mailbox.org>

# fully upgrade system
RUN apt update && apt full-upgrade -y

# set locales to be able to use UTF-8
RUN apt install -y locales vim

# Set the locale
RUN sed -i '/en_US.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8     


# create your linux user (we will use user vagrant) and set password
RUN useradd --create-home --shell /bin/bash vagrant
RUN echo vagrant:vagrant | chpasswd

# set up sudo, add your user to sudoers group and allow to act as root and postgres user without asking password
RUN apt install -y sudo
RUN usermod -a -G sudo vagrant
RUN echo "vagrant ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/user && echo "vagrant ALL=(postgres) NOPASSWD:ALL" >> /etc/sudoers.d/user && chmod 0440 /etc/sudoers.d/user

# install: build tools
RUN apt update && apt install -y make gcc git wget gnupg ca-certificates curl

# add [pgdg](https://www.postgresql.org/download/linux/debian/) repository
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ buster-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN wget --quiet --no-check-certificate https://www.postgresql.org/media/keys/ACCC4CF8.asc
RUN apt-key add ACCC4CF8.asc

# install latest postgres & postgis, corresponding postgresql-server-dev, plpython3 and python3-numpy
RUN apt update && apt install -y postgresql-12 postgresql-contrib-12 \
    postgresql-12-postgis-3 postgresql-12-postgis-3-scripts \
    postgresql-server-dev-12 \
    postgresql-plpython3-12 python3-scipy \
    python3-psycopg2 \
    postgresql-12-pgtap libtap-formatter-junit-perl \
    && apt-get clean

# db configuration (turn off JIT)
# allow passwordless logins from local
RUN sed -i "s/#jit = on/jit = off/" /etc/postgresql/12/main/postgresql.conf && sed -i "s/#max_locks_per_transaction = 64/max_locks_per_transaction = 256/" /etc/postgresql/12/main/postgresql.conf && sed -i "s/peer/trust/g" /etc/postgresql/12/main/pg_hba.conf

# db migration tool
RUN curl -fsSL -o /usr/local/bin/dbmate https://github.com/amacneil/dbmate/releases/latest/download/dbmate-linux-amd64 && chmod +x /usr/local/bin/dbmate

# install junit xml exporter for tap
RUN curl -L http://cpanmin.us | perl - --self-upgrade && cpanm XML::Simple && cpanm Test::Deep && cpanm TAP::Harness::JUnit

# Set the working directory to app home directory
WORKDIR /home/vagrant

# Specify the user to execute all commands below
USER vagrant


